from flask import Flask

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import db
from models import User

uri = 'sqlite:///models/auth.db'
app = Flask(__name__)


engine = create_engine(uri, echo=True)

db.metadata.bind = engine

DBSession = sessionmaker(bind=engine)

session = DBSession()


@app.route("/login", methods=['GET', 'POST'])
def login():
    pass


@app.route("/logout")
def logout():
    pass


@app.route("/signup", methods=['GET', 'POST'])
def signup():
    pass


@app.route("/home", methods=["GET", "POST"])
def home():
    pass


@app.route("/add")
def add_user():
    user = User('Hakim', 'BOURMEL', 'hakimtighouza@yahoo.fr', 'tighouza')
    session.add(user)
    session.commit()

    return("Insert OK")


if __name__ == '__main__':

    app.run(debug=True)
