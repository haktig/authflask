
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

from passlib.hash import pbkdf2_sha512

uri = 'sqlite:///auth.db'

db = declarative_base()


class User(db):
    __tablename__ = 'users'
    uid = Column(Integer, primary_key=True, autoincrement=True)
    firstname = Column(String(100), nullable=False)
    lastname = Column(String(100), nullable=False)
    email = Column(String(120), nullable=False, unique=True)
    passwd = Column(String(135), nullable=False)

    def __init__(self, firstname, lastname, email, passwd):
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.__set_password(passwd)

    def __set_password(self, passwd):
        self.passwd = pbkdf2_sha512.hash(passwd)

    def check_password(self, passwd):
        return pbkdf2_sha512.verify(passwd, self.passwd)


if __name__ == '__main__':
    engine = create_engine(uri, echo=True)
    db.metadata.create_all(engine)
